package com.daddyscode.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.daddyscode.dao.ImplementDao;
import com.daddyscode.dto.UserModel;

/**
 * Servlet implementation class AddBonusServlet
 */
public class AddBonusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	//missing response if exception generate
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//implement query
		ImplementDao implementDao=new ImplementDao();
		try {
			boolean result=implementDao.updateBonusQuery();
			if (result==true) {
				System.out.println("bonus updated");
				response.sendRedirect(request.getContextPath()+"/home.jsp");
			}
			else {
				System.out.println("bonus not updated");
				response.sendRedirect(request.getContextPath()+"/home.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
	}

}
