package com.daddyscode.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.daddyscode.dao.ImplementDao;
import com.daddyscode.dto.UserModel;
import com.daddyscode.utils.Errors;

/**
 * Servlet implementation class AddUserServlet
 */
public class AddUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	//missing response if exception generate
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//get data from form
		String username=request.getParameter("addusername");
		String password=request.getParameter("addpassword");
		String joindate=request.getParameter("addjoindate");
		String salary=request.getParameter("addsalary");
		
		//set data in model
		UserModel userModel=new UserModel();
		userModel.setUsername(username);
		userModel.setPassword(password);
		userModel.setJoindate(joindate);
		userModel.setSalary(salary);
		
		//implement query
		ImplementDao implementQueries=new ImplementDao();
		try {
			boolean result=implementQueries.addUserQuery(userModel);
			//result
			if (result==true) {
				System.out.println("user added");
				response.sendRedirect(request.getContextPath()+"/home.jsp");
			}
			else {
				System.out.println("user not added");
				request.setAttribute(Errors.MESSAGE.toString(), "user not added.try again.");
				request.getRequestDispatcher("/home.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

}
