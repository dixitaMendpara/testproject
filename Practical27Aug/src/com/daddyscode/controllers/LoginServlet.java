package com.daddyscode.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.daddyscode.dao.ImplementDao;
import com.daddyscode.dto.UserModel;
import com.daddyscode.utils.Errors;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	//missing response if exception generate
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession httpSession=request.getSession();
		//get data from form
		String username=request.getParameter("loginusername");
		String password=request.getParameter("loginpassword");
		
		//set in model
		UserModel userModel=new UserModel();
		userModel.setUsername(username);
		userModel.setPassword(password);
		
		//implement query
		ImplementDao implementQueries=new ImplementDao();
		try {
			
			userModel=implementQueries.LoginQuery(userModel);
			
			//result
			if (userModel!=null) {
				httpSession.setAttribute("user", userModel.getUsername());
				response.sendRedirect(request.getContextPath()+"/home.jsp");
			}
			else {
				request.setAttribute(Errors.MESSAGE.toString(), "username and password wrong");
				System.out.println("username and password wrong");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

}
