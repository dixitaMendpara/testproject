package com.daddyscode.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;

import com.daddyscode.dto.UserModel;
import com.daddyscode.utils.CloseConnections;
import com.daddyscode.utils.Queries;
import com.daddyscode.utils.Singleton;
//mission close con
public class ImplementDao {

	public UserModel LoginQuery(UserModel userModel) throws ClassNotFoundException, SQLException {
		ResultSet resultSet=null;
		PreparedStatement preparedStatement=Singleton.getInstance().getConnection().prepareStatement(Queries.LOGIN_QUERY);
		preparedStatement.setString(1, userModel.getUsername());
		preparedStatement.setString(2, userModel.getPassword());
		resultSet=preparedStatement.executeQuery();
		if(resultSet.next()) {
			userModel.setUsername(resultSet.getString(2));
		}
		else {
			userModel=null;
		}
		CloseConnections.close(preparedStatement, resultSet);
		return userModel;
	}
	
	public ArrayList<UserModel> getAllDataQuery() throws ClassNotFoundException, SQLException {
		ResultSet resultSet=null;
		ArrayList<UserModel> arrayListOfuserModels=new ArrayList<UserModel>();
		PreparedStatement preparedStatement=Singleton.getInstance().getConnection().prepareStatement(Queries.GETALLDATA_QUERY);
		resultSet=preparedStatement.executeQuery();
		while(resultSet.next()) {
			UserModel userModel=new UserModel();
			userModel.setUsername(resultSet.getString(2));
			userModel.setJoindate(resultSet.getString(4));
			userModel.setSalary(resultSet.getString(5));
			userModel.setBonus(resultSet.getString(6));
			arrayListOfuserModels.add(userModel);
		}
		CloseConnections.close(preparedStatement, resultSet);
		return arrayListOfuserModels;
	}
	
	public boolean addUserQuery(UserModel userModel) throws ClassNotFoundException, SQLException {
		int result=0;
		PreparedStatement preparedStatement=Singleton.getInstance().getConnection().prepareStatement(Queries.ADDUSER_QUERY);
		preparedStatement.setString(1, userModel.getUsername());
		preparedStatement.setString(2, userModel.getPassword());
		preparedStatement.setString(3, userModel.getSalary());
		result=preparedStatement.executeUpdate();
		return result>0?true:false;
	}
	
	public String countBonus(String username) throws ClassNotFoundException, SQLException, ParseException {
		ResultSet resultSet;
		String bonus="0";
		PreparedStatement preparedStatement=Singleton.getInstance().getConnection().prepareStatement(Queries.GETUSERDATA_QUERY);
		preparedStatement.setString(1, username);
		resultSet=preparedStatement.executeQuery();
		while(resultSet.next()) {
			String salary=resultSet.getString(5);
			String joindateinstring=resultSet.getString(4);
			if (joindateinstring==null) {
				bonus="0";
			}
			else {
				//convert string to localdate
				Date joindateDate=new SimpleDateFormat("dd/MM/yyyy").parse(joindateinstring);
				LocalDate joindate = joindateDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				
				//current date
				LocalDate nowdate = LocalDate.now(); 
				
				//difference between dates
			    long months = ChronoUnit.MONTHS.between(joindate, nowdate);
			    System.out.println("month="+months);
			    
			    //bonus
			    
			    long sal=Long.parseLong(salary);
			    if (months>12) {
					months=12;
				}
			    long longbonus=(sal*months)/(12);
			    bonus=String.valueOf(longbonus);
			    System.out.println("bonus=="+bonus);
			}
		    
		}
		
		return bonus;
	}
	public boolean updateBonusQuery() throws ClassNotFoundException, SQLException, ParseException {
		int result=0;
		ResultSet resultSet=null;
		PreparedStatement preparedStatement=Singleton.getInstance().getConnection().prepareStatement(Queries.GETALLDATA_QUERY);
		resultSet=preparedStatement.executeQuery();
		while(resultSet.next()) {
			UserModel userModel=new UserModel();
			userModel.setUsername(resultSet.getString(2));
			userModel.setBonus(resultSet.getString(6));
			String bonus=countBonus(resultSet.getString(2));
			PreparedStatement preparedStatement2=Singleton.getInstance().getConnection().prepareStatement(Queries.UPDATEBONUS_QUERY);
			preparedStatement2.setString(1, bonus);
			preparedStatement2.setString(2, resultSet.getString(2));
			result=preparedStatement2.executeUpdate();
		}
		return result>0?true:false;
	}
	
}
