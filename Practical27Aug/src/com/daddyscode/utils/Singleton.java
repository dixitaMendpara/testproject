package com.daddyscode.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Singleton implements Credentials{
	private static Singleton instance;
	
	private Singleton() {}
	
	public static Singleton getInstance() {
		if (instance==null) {
			instance=new Singleton();
		}
		return instance;
	}
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection connection=null;
		if (connection==null || connection.isClosed()) {
			Class.forName(DRIVER);
			connection=DriverManager.getConnection(PATH,USERNAME,PASSWORD);
		}
		return connection;
	}
}
