package com.daddyscode.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CloseConnections {
	public static void close(ResultSet rs) {
		if (rs != null) {
	        try {
	            rs.close();
	        } catch (SQLException e) { System.out.println(e.getMessage());}
	    }
	}
	public static void close(PreparedStatement ps) {
		if (ps != null) {
	        try {
	            ps.close();
	        } catch (SQLException e) { System.out.println(e.getMessage());}
	    }
	}
	
	public static void close(PreparedStatement ps,ResultSet rs) {
		close(ps);
		close(rs);
	}
	
	public static void close(Connection conn) {
		if (conn != null) {
	        try {
	            conn.close();
	        } catch (SQLException e) { System.out.println(e.getMessage());}
	    }
	}
}
