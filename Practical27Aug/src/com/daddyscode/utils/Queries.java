package com.daddyscode.utils;

public class Queries {
	public static final String LOGIN_QUERY="select * from usertable where username=? AND password=?";
	public static final String ADDUSER_QUERY="insert into usertable (username,password,salary) values (?,?,?)";
	public static final String GETALLDATA_QUERY="select * from usertable";
	public static final String GETUSERDATA_QUERY="select * from usertable where username=?";
	public static final String UPDATEBONUS_QUERY="update usertable set bonus=? where username=?";
}
