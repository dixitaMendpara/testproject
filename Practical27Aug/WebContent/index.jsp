<%@page import="com.daddyscode.utils.Errors"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
    response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
    response.setDateHeader("Expires", -1); //Causes the proxy cache to see the page as "stale"
    response.setHeader("Expires", "0");
    response.setHeader("Pragma", "no-cache");

    if(request.getAttribute(Errors.MESSAGE.toString())!=null){
		String Error=(String)session.getAttribute(Errors.MESSAGE.toString());
		out.print("<font style='color:red;font-size: 15px'>"+Error+"</font>");
		}
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>login</title>
<link rel="stylesheet" href="css/style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
	<div class="forms">
		<h1>Login</h1>
		<form id="myform" action="LoginServlet" method="post" onsubmit="return logincheck();">
		<input type="text" name="loginusername" id="loginusername" placeholder="Enter Username" autofocus>
		<input type="password" name="loginpassword" id="loginpassword" placeholder="Enter Password">
		<input type="submit" value="login">
		</form>
	</div>
	
<script src="js/login.js"></script>
</body>
</html>