<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.daddyscode.dto.UserModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.daddyscode.dao.ImplementDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    	response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", -1); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Expires", "0");
        response.setHeader("Pragma", "no-cache");
        
        try {
    		if (session == null || session.getAttribute("user") == null) {
    	out.print("no user");
    	response.sendRedirect(request.getContextPath() + "/index.jsp");
    	return;
    		}
    	} catch (Exception e) {
    		response.sendRedirect(request.getContextPath() + "/index.jsp");
    	}
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>home</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- div for buttons -->
	<div id="buttons">
		
		<!-- add user button -->
		<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#AddUserModal">Add User</button>
		
		<!-- add bonus button -->
		<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#AddBonusModal">Add Bonus</button>
		
	</div>
	
	<div id="logout">
	
	<!-- logout -->
		<a href="LogoutServlet">Logout</a>
	
	</div>
	
	
	<!-- add user modal -->
	<div id="AddUserModal" class="modal fade" role="dialog">
	 <div class="modal-dialog">
	
	   <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add User</h4>
	      </div>
	      <div class="modal-body">
	        <form action="AddUserServlet" method="post" onsubmit="return checkfields();">
	        <table id="mytable">
	        <tr>
	        <td><label>Name</label></td>
	        <td><input type="text" name="addusername" placeholder="Enter Username"></td>
	        </tr>
	        <tr>
	        <td><label>Password</label></td>
	        <td><input type="password" name="addpassword" placeholder="Enter Password"></td>
	        </tr>
	        <tr>
	        <td><label>Salary</label></td>
	        <td><input type="text" name="addsalary" placeholder="Enter Salary"></td>
	        </tr>
	        </table>
	        <input type="submit" value="Add">
	        </form>
	      </div>
	    </div>
	
	  </div>
	</div>
	
	

<!-- add bonus modal -->
	<div id="AddBonusModal" class="modal fade" role="dialog">
	 <div class="modal-dialog">
	
	   <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add Bonus</h4>
	      </div>
	      <div class="modal-body">
	      	<form action="AddBonusServlet" method="post">
	      	<p>are you sure you want to update bonus?</p>
	        <button type="submit">Add</button>
	        <button type="button" data-dismiss="modal">close</button>
	        </form>
	        
	      </div>
	    </div>
	
	  </div>
	</div>
	
	<!-- data from database -->
	
	<table id="mytable">
		<tr>
		    <th>Username</th>
		    <th>JoinDate</th>
		    <th>Salary</th>
		    <th>Bonus</th>
		</tr>
		<%
			ArrayList<UserModel> arrayListOfuserModels=new ImplementDao().getAllDataQuery();
				for(UserModel userModels:arrayListOfuserModels){
		%>
		<tr>
            <td class="contact-firstcol"> <label class="model" for="message"><%=userModels.getUsername() %></label> </td>
            <td class="contact-firstcol"> <label class="model" for="message"><%=userModels.getJoindate() %></label> </td>
            <td class="contact-firstcol"> <label class="model" for="message"><%=userModels.getSalary() %></label> </td>
            <td class="contact-firstcol"> <label class="model" for="message"><%=userModels.getBonus() %></label> </td>
        </tr>
	<% } %>
	</table>
	
<script type="text/javascript" src="js/home.js"></script>
</body>
</html>